import { Component, OnInit } from '@angular/core';
import { Expense } from '../shared/models/expense';
import { ExpenseService } from '../shared/services/expense.service';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.page.html',
  styleUrls: ['./add-expense.page.scss']
})
export class AddExpensePage implements OnInit {
  expense: Expense = {
    _id: null,
    item: null,
    price: null
  };

  constructor(private expenseService: ExpenseService, private router: Router) {}

  ngOnInit() {}

  onSubmit() {
    this.expense._id = new Date().getTime().toString();
    this.expenseService
      .addExpense(this.expense)
      .pipe(take(1))
      .subscribe(() => {
        this.router.navigate(['/expenses']);
      });
  }
}
