import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { ToastController, LoadingController } from '@ionic/angular';
import { createToast } from '../shared/utils/toastUtils';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  user = {
    user: '',
    password: ''
  };
  loading: HTMLIonLoadingElement;
  rememberMe: boolean = false;

  constructor(
    private auth: AuthService,
    private toastController: ToastController,
    private router: Router,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {
    this.auth.token.pipe(take(1)).subscribe(token => {
      if (token) this.router.navigate(['/expenses']);
    });
  }

  async onSubmit() {
    try {
      await this.load(true);
      await this.auth.authenticate(this.user, this.rememberMe).toPromise();
      this.router.navigateByUrl('/expenses');
    } catch (error) {
      const issue = error.error.issue[0].error;
      const toast = await this.toastController.create(
        createToast(issue, 'danger')
      );
      toast.present();
    } finally {
      await this.load(false);
    }
  }

  async load(isLoading: boolean) {
    if (isLoading) {
      this.loading = await this.loadingCtrl.create({
        spinner: 'dots'
      });
      await this.loading.present();
    } else {
      await this.loading.dismiss();
    }
  }
}
