import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from '../shared/shared.module';
import { ExpenseListComponent } from './components/expense-list/expense-list.component';
import { ExpenseViewComponent } from './components/expense-view/expense-view.component';
import { ExpensesPage } from './expenses.page';
import { ExpenseService } from '../shared/services/expense.service';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';

const routes: Routes = [
  {
    path: '',
    component: ExpensesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    ChartsModule
  ],
  declarations: [ExpensesPage, ExpenseViewComponent, ExpenseListComponent],
  providers: []
})
export class ExpensesPageModule {}
