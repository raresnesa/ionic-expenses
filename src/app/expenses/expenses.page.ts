import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExpenseService } from '../shared/services/expense.service';
import { AuthService } from '../shared/services/auth.service';
import { Observable, Subscription } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.page.html',
  styleUrls: ['./expenses.page.scss']
})
export class ExpensesPage {
  constructor(
    private auth: AuthService,
    private expenseService: ExpenseService,
    private router: Router
  ) {}

  async logout() {
    this.expenseService.disconnectWs();
    await this.auth.logout();
    this.router.navigate(['/']);
  }

  ionViewWillEnter() {
    this.auth.token.pipe(take(1)).subscribe(token => {
      this.expenseService.connectAndSyncIfNeeded(token);
    });
  }
}
