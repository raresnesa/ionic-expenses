import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';

import { AuthService } from '../../../shared/services/auth.service';
import { ExpenseService } from '../../../shared/services/expense.service';
import { Expense } from '../../../shared/models/expense';

@Component({
  selector: 'app-expense-list',
  templateUrl: './expense-list.component.html',
  styleUrls: ['./expense-list.component.scss']
})
export class ExpenseListComponent implements OnInit, OnDestroy {
  expenses: Expense[];
  expenses$: Observable<Expense[]>;
  subscriptions: Subscription[] = [];

  // chart
  chartLabels: string[] = [
    '< 10lei',
    '10-100 lei',
    '100-1000 lei',
    '> 1000 lei'
  ];
  chartData: number[] = [100, 3, 50, 25];

  constructor(
    private expenseService: ExpenseService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.expenses$ = this.expenseService.getExpenses().pipe(
      tap(expenses => {
        if (!expenses) return;
        const cd = [0, 0, 0, 0];
        expenses.forEach(expense => {
          const { price } = expense;
          if (price < 10) cd[0]++;
          if (price >= 10 && price < 100) cd[1]++;
          if (price >= 100 && price < 1000) cd[2]++;
          if (price > 1000) cd[3]++;
        });
        setTimeout(() => (this.chartData = cd));
      })
    );
    // this.subscriptions.push(this.getExpenses());
  }

  ngOnDestroy(): void {
    console.log('On expenses list destroy');
    // this.expenseService.disconnectWs();
    this.subscriptions.forEach(s => s.unsubscribe);
  }

  getExpenses() {
    return this.expenseService.getExpenses().subscribe(expenses => {
      this.expenses = expenses;
    });
  }

  deleteExpense(id: string) {
    this.expenseService
      .deleteExpense(id)
      .pipe(take(1))
      .subscribe();
  }
}
