import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Expense } from '../../../shared/models/expense';
import { ExpenseService } from '../../../shared/services/expense.service';
import { take } from 'rxjs/operators';
import { ItemSliding } from '@ionic/angular';

@Component({
  selector: 'app-expense-view',
  templateUrl: './expense-view.component.html',
  styleUrls: ['./expense-view.component.scss']
})
export class ExpenseViewComponent implements OnInit {
  @Input()
  expense: Expense;

  // @Output()
  // onDelete: EventEmitter<string> = new EventEmitter();

  constructor(private expenseService: ExpenseService) {}

  ngOnInit() {}

  delete(slidingItem: ItemSliding) {
    slidingItem.close();
    this.expenseService
      .deleteExpense(this.expense._id)
      .pipe(take(1))
      .subscribe();
    // this.onDelete.emit(this.expense._id);
  }
}
