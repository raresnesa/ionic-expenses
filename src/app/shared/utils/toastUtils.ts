import { ToastOptions } from '@ionic/core';

export const createToast = (
  message: string,
  color: string = 'primary'
): ToastOptions => {
  const toast: ToastOptions = {
    message,
    showCloseButton: true,
    position: 'bottom',
    duration: 3000,
    color
  };

  return toast;
};
