// export const base: string = '192.168.43.49:3000';
// export const base: string = '192.168.1.7:3000';
export const base: string = 'localhost:3000';
export const baseHttp: string = `http://${base}/api`;
export const baseWs: string = `ws://${base}/api`;
