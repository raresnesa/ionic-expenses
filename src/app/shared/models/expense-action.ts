import { ActionType } from './action-type';
import { Expense } from './expense';

export interface ExpenseAction {
  actionType: ActionType;
  expense?: Expense;
  id?: string;
}
