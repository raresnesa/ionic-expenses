export interface Expense {
  item: string;
  price: number;
  _id?: string;
}
