import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { take, tap } from 'rxjs/operators';

import { Expense } from '../models/expense';
import { createToast } from '../utils/toastUtils';
import { baseHttp, baseWs } from '../utils/urls';
import { AuthService } from './auth.service';
import { ConnectivityService } from './connectivity.service';
import { ExpenseAction } from '../models/expense-action';
import { ActionType } from '../models/action-type';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {
  private sorter = (e1, e2) => e2.price - e1.price;
  private ws: WebSocket;

  expenses: Expense[] = null;
  expensesSubject: BehaviorSubject<Expense[]> = new BehaviorSubject(
    this.expenses
  );

  connected: boolean;
  connected$: Observable<boolean>;
  expenseActions: ExpenseAction[];

  constructor(
    private http: HttpClient,
    private toastController: ToastController,
    private storage: Storage,
    private authService: AuthService,
    private connectivityService: ConnectivityService
  ) {
    this.connected$ = this.connectivityService.connected$;
    this.initSync();
  }

  getExpenses(): Observable<Expense[]> {
    if (this.expenses === null) {
      this.storage.get('expenses').then(expensesString => {
        this.expenses = JSON.parse(expensesString) || [];
        this.expensesSubject.next(this.expenses);
        console.log('load expenses from disk');
      });
    }
    return this.expensesSubject.asObservable();
  }

  connectAndSyncIfNeeded(token: string): any {
    if (!this.ws) {
      this.connectWebsocket(token);
      this.syncExpenses();
    }
  }

  deleteExpense(id: string) {
    if (this.connected) {
      return this.http.delete(`${baseHttp}/expense/${id}`);
    } else {
      this.deleteExpenseLocal(id);
      return from(
        this.addAction({
          actionType: ActionType.DELETE,
          id
        })
      );
    }
  }

  addExpense(expense: Expense) {
    if (this.connected) {
      return this.http.post(`${baseHttp}/expense`, expense);
    } else {
      this.addExpenseLocal(expense);
      return from(
        this.addAction({
          actionType: ActionType.ADD,
          expense
        })
      );
    }
  }

  private async initSync() {
    this.expenseActions = (await this.storage.get('expenseActions')) || [];
    this.connected$.subscribe(connected => {
      this.connected = connected;
      this.authService.token.pipe(take(1)).subscribe(token => {
        if (!token) return;

        if (connected) {
          this.syncExpenses();
          this.connectWebsocket(token);
          this.checkAndExecuteActions();
        } else {
          this.disconnectWs();
        }
      });
    });
  }

  private async checkAndExecuteActions() {
    console.log('execute actions');
    while (this.expenseActions.length != 0 && this.connected) {
      const action = this.expenseActions.shift();
      switch (action.actionType) {
        case ActionType.ADD:
          this.addExpense(action.expense)
            .pipe(take(1))
            .subscribe();
          break;
        case ActionType.DELETE:
          this.deleteExpense(action.id)
            .pipe(take(1))
            .subscribe();
          break;
      }
      this.storage.set('expenseActions', this.expenseActions);
    }
  }

  async syncExpenses() {
    await this.http
      .get<Expense[]>(`${baseHttp}/expense`)
      .pipe(
        tap(expenses => {
          this.expenses = expenses;
          return expenses.sort(this.sorter);
        })
      )
      .toPromise();

    await this.saveExpenses();
    this.expensesSubject.next(this.expenses);
    this.showToast('Expenses updated!');
    console.log('loaded expenses from server');
  }

  connectWebsocket(token: string) {
    const ws = new WebSocket(baseWs);
    this.ws = ws;

    ws.onopen = () => {
      console.log('Websocket connection opened');
      ws.send(token);
    };

    ws.onerror = issue => {
      console.log(`Websocket error: ${issue}`);
      this.showToast('Websocket error', 'danger');
    };

    ws.onclose = () => {
      console.log('Websocket connection closed');
      this.showToast('Websocket connection closed', 'warning');
    };

    ws.onmessage = message => {
      const event = JSON.parse(message.data);
      const data: Expense = event.payload;
      console.log(`new event : ${event.event}`);

      switch (event.event) {
        case 'expense/created':
          this.addExpenseLocal(data);
          break;
        case 'expense/updated':
          this.updateExpenseLocal(data);
          break;
        case 'expense/deleted':
          this.deleteExpenseLocal(data._id);
          break;
      }
    };
  }

  disconnectWs() {
    if (!this.ws) return;
    this.ws.close();
    this.ws = null;
  }

  private async showToast(message: string, color: string = 'primary') {
    const toast = await this.toastController.create(
      createToast(message, color)
    );
    toast.present();
  }

  private saveExpenses(): Promise<any> {
    return this.storage.set('expenses', JSON.stringify(this.expenses));
  }

  private addAction(expenseAction: ExpenseAction) {
    this.expenseActions.push(expenseAction);
    return this.storage.set('expenseActions', this.expenseActions);
  }

  private addExpenseLocal(expense: Expense) {
    if (!this.expenses.find(exp => exp._id === expense._id)) {
      this.expenses = this.expenses.concat([expense]).sort(this.sorter);
      this.showToast('A new expense was created', 'success');
    }
    this.saveAndNotify();
  }

  private deleteExpenseLocal(id: string) {
    this.expenses = this.expenses
      .filter(exp => exp._id !== id)
      .sort(this.sorter);
    this.showToast('An expense was deleted', 'danger');
    this.saveAndNotify();
  }

  private updateExpenseLocal(expense: Expense) {
    this.expenses = this.expenses
      .map(exp => (exp._id === expense._id ? expense : exp))
      .sort(this.sorter);
    this.showToast('An expense was updated', 'secondary');
    this.saveAndNotify();
  }

  private saveAndNotify() {
    this.saveExpenses().then(() => this.expensesSubject.next(this.expenses));
  }
}
