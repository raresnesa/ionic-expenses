import {
  ErrorHandler,
  Injectable,
  ChangeDetectorRef,
  Injector,
  NgZone
} from '@angular/core';
import { ToastController } from '@ionic/angular';
import { createToast } from '../utils/toastUtils';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AppErrorHandler implements ErrorHandler {
  constructor(
    private toastController: ToastController,
    private injector: Injector,
    private auth: AuthService,
    private ngZone: NgZone
  ) {}

  handleError(error) {
    console.error(error);

    if (error instanceof HttpErrorResponse) {
      this.auth.logout();
      this.toastController
        .create(createToast(error.error.message, 'danger'))
        .then(toast => toast.present());
    }

    this.toastController
      .create(createToast('An unexpected error occured', 'danger'))
      .then(toast => toast.present());
  }
}
