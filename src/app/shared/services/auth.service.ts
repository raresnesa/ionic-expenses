import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseHttp } from '../utils/urls';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { from, of, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { ExpenseService } from './expense.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  _token: string;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private router: Router,
    private ngZone: NgZone
  ) {}

  get token() {
    if (this._token) return of(this._token);
    else {
      return from(this.storage.get('token'));
    }
  }

  authenticate(
    credentials: { user: string; password: string },
    rememberMe: boolean = false
  ) {
    return this.http.post(`${baseHttp}/auth/login`, credentials).pipe(
      tap((response: any) => {
        this._token = response.token;
        if (rememberMe) {
          this.storage.set('token', response.token);
        }
      })
    );
  }

  async logout() {
    await this.storage.remove('token');
    this._token = null;
    // this.ngZone.run(() => this.router.navigateByUrl('/')).then();
  }
}
