import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { Platform } from '@ionic/angular';
import { ConnectionService } from 'ng-connection-service';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConnectivityService {
  connectedSubject: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor(
    private network: Network,
    private platform: Platform,
    private connectionService: ConnectionService
  ) {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        // make native API calls
        console.log('cordova');
        this.initSyncCordova();
      } else {
        // fallback to browser APIs
        console.log('not cordova');
        this.initSyncBrowser();
      }
    });
  }

  get connected$() {
    return this.connectedSubject.asObservable();
  }

  initSyncCordova() {
    this.network.onConnect().subscribe(() => {
      console.log('Got connection (mobile)');
      this.connectedSubject.next(true);
    });

    this.network.onDisconnect().subscribe(() => {
      console.log('Lost connection (mobilenado )');
      this.connectedSubject.next(false);
    });

    if (this.network.type === 'none') {
      this.connectedSubject.next(false);
    }
  }

  initSyncBrowser() {
    this.connectionService.monitor().subscribe(connected => {
      if (connected) {
        console.log('Got connection (browser)');
        this.connectedSubject.next(true);
      } else {
        console.log('Lost connection (browser)');
        this.connectedSubject.next(false);
      }
    });
  }
}
